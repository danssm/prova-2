package Model;

import java.util.ArrayList;

public class Paciente extends Usuario {
	
	private String idade;
	private String sintoma;
	private String planoSaude;
	private ArrayList<ConsultaMedica> listaConsulta;

	
	
	public String getPlanoSaude() {
		return planoSaude;
	}

	public void setPlanoSaude(String planoSaude) {
		this.planoSaude = planoSaude;
	}
	
	public String getSintoma() {
		return sintoma;
	}
	public void setSintoma(String sintoma) {
		this.sintoma = sintoma;
	}
	
	public String getIdade() {
		return idade;
	}
	public void setIdade(String idade) {
		this.idade = idade;
	}
	
	public Paciente(String nome, String sintoma) {
		super(nome);
		this.sintoma = sintoma;
		listaConsulta = new ArrayList<ConsultaMedica>();
	}
	
	public void adicionar(ConsultaMedica umaConsulta) {
		listaConsulta.add(umaConsulta);
	}
	
	public void remover(ConsultaMedica umaConsulta) {
		listaConsulta.remove(umaConsulta);
	}
	
	public ConsultaMedica buscarConsulta(String umaData, String umHorario) {
		if(listaConsulta.size()!=0) {
			for (ConsultaMedica umaConsulta : listaConsulta) {
				if (umaConsulta.getData().equalsIgnoreCase(umaData)==true){
					if (umaConsulta.getHorario().equalsIgnoreCase(umHorario)==true){
						return umaConsulta;
					}
				}
			}
		}
		return null;
		
	}
	
	public String listarConsultas() {
		String ConsultasCadastrados = ("Consultas com " + getNome());
		for(ConsultaMedica umaConsulta : listaConsulta) {
			ConsultasCadastrados = (ConsultasCadastrados + "\n" + umaConsulta.getData() + " - " + umaConsulta.getHorario());
		}
		return ConsultasCadastrados;
	} 
	
	public String ficharConsulta (ConsultaMedica umaConsulta) {
		String stringGigante = ("Data : " + umaConsulta.getData() + "\n" +
								"Horario : " + umaConsulta.getHorario() + "\n" + "Diagnóstico : " + umaConsulta.getDiagnostico() );
		return stringGigante;
	}

	
}
