package Model;

import java.util.ArrayList;

public class Medico extends Usuario{

	private String registroMedico;
	private String especialidade;
    private ArrayList<ConsultaMedica> listaConsulta;
	
	
	public Medico(String nome, String especialidade) {
		super(nome);
		this.especialidade  = especialidade;
		listaConsulta = new ArrayList<ConsultaMedica>();
		
	}

	public String getRegistroMedico() {
		return registroMedico;
	}

	public void setRegistroMedico(String registroMedico) {
		this.registroMedico = registroMedico;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public void adicionar(ConsultaMedica umaConsulta) {
		listaConsulta.add(umaConsulta);
	}
	
	public void remover(ConsultaMedica umaConsulta) {
		listaConsulta.remove(umaConsulta);
	}
	
	public ConsultaMedica buscarConsulta(String umaData, String umHorario) {
		if(listaConsulta.size()!=0) {
			for (ConsultaMedica umaConsulta : listaConsulta) {
				if (umaConsulta.getData().equalsIgnoreCase(umaData)==true){
					if (umaConsulta.getHorario().equalsIgnoreCase(umHorario)==true){
						return umaConsulta;
					}
				}
			}
		}
		return null;
	}
	
	public String listarConsultas() {
		String ConsultasCadastrados = ("Consultas com " + getNome());
		for(ConsultaMedica umaConsulta : listaConsulta) {
			ConsultasCadastrados = (ConsultasCadastrados + "\n" + umaConsulta.getData() + " - " + umaConsulta.getHorario());
		}
		return ConsultasCadastrados;
	} 
	
	public String ficharConsulta (ConsultaMedica umaConsulta) {
		String stringGigante = ("Data : " + umaConsulta.getData() + "\n" +
								"Horario : " + umaConsulta.getHorario() + "\n" + "Diagnóstico : " + umaConsulta.getDiagnostico() );
		return stringGigante;
	}

	

}
