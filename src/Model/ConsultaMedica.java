package Model;

public class ConsultaMedica {
	
	
	private String data;
	private String horario;
	private String diagnostico;

	
	
	public ConsultaMedica(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public boolean equalization(ConsultaMedica umaConsulta) {
		if(umaConsulta.getData()==this.data) {
			if(umaConsulta.getHorario()==this.horario) {
				if(umaConsulta.getDiagnostico()==this.diagnostico) {
					return true;
				}
			}
		}
		return false;
	}
	
}
