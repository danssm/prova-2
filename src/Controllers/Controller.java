package Controllers;

import java.util.ArrayList;

import Model.ConsultaMedica;
import Model.Medico;
import Model.Paciente;


public class Controller {

	private ArrayList<Paciente> listaPaciente;
	private ArrayList<Medico> listaMedico;

	public Controller() {
		listaPaciente = new ArrayList<Paciente>();
		listaMedico = new ArrayList<Medico>();	
	}
	
	
	public void adicionar(Medico umMedico) {
		
		listaMedico.add(umMedico);
	}
	
	
	public void adicionar(Paciente umPaciente) {
		listaPaciente.add(umPaciente);
	}
	
	public void remover(Medico umMedico ) {
		listaMedico.remove(umMedico);
	}
	
	public void remover(Paciente umPaciente) {
		listaPaciente.remove(umPaciente);
	}
	
	public Medico buscarMedico(String umNome) {
		if(listaMedico.size()!=0) {
			for (Medico umMedico : listaMedico) {
				if (umMedico.getNome().equalsIgnoreCase(umNome)==true){
					return umMedico;
				}
			}
		}
		//System.out.println("Não foi cadastrado nenhum médico.");
		return null;
	}
	
	public Paciente buscarPaciente(String nome) {
		if(listaMedico.size()!=0) {
			for (Paciente umPaciente : listaPaciente) {
				if (umPaciente.getNome()=="nome"){
					return umPaciente;
				}
			}
		}
		//System.out.println("Não foi cadastrado nenhum paciente.");
		return null;
		
	}
	
	public int qualPosicaoMed(String umNome) {
		int n = 0;
		for (Medico umMedico : listaMedico) {
			if (umMedico.getNome()=="umNome"){
				return n;
			}
			n = n + 1;
		}
		return -1;
	}
	
	public int qualPosicaoPac(String umNome) {
		int n = 0;
		for (Paciente umPaciente : listaPaciente) {
			if (umPaciente.getNome()=="umNome"){
				
				return n;
			}
			n = n + 1;
		}
		return -1;
	}
	
	public void reAdicionar(int n, Medico umMed) {
		listaMedico.add(n, umMed);
	}
	
	public void reAdicionar(int n, Paciente umPac) {
		listaPaciente.add(n, umPac);
	}

	public String listarUsuarios() {
	
		String UsuarioCadastrados = "Médicos";
		int n = 1;
		for(Medico umMedico : listaMedico) {
			UsuarioCadastrados = (UsuarioCadastrados + "\n" + Integer.toString(n) + ") " + umMedico.getNome());
			n = n + 1;
		}
		UsuarioCadastrados = (UsuarioCadastrados + "\n\n" + "Pacientes");
		n = 1;
		for(Paciente umPaciente : listaPaciente) {
			UsuarioCadastrados = (UsuarioCadastrados + "\n" + Integer.toString(n) + ") " + umPaciente.getNome());
			n = n + 1;
		}
		return UsuarioCadastrados;
	} 
	
	public String fichaUsuario (Medico umMedico) {
		String stringGigante = ("Médico" + "\n" + "Nome : " + umMedico.getNome() + "\n" +
								"Especialidade : " + umMedico.getEspecialidade() + "\n" + "RG : " + umMedico.getRg() + "\n" +
								"CPF : " + umMedico.getCpf() + "\n" + "Registro Médico : " + umMedico.getRegistroMedico() );
		return stringGigante;
	}
	
	public String fichaUsuario (Paciente umPaciente) {
		String stringGigante = ("Paciente" + "\n" + "Nome : " + umPaciente.getNome() + "\n" +
								"Sintoma : " + umPaciente.getIdade() + "\n" + "RG : " + umPaciente.getRg() + "\n" +
								"CPF : " + umPaciente.getCpf() + "\n" + "Plano de Saúde : " + umPaciente.getPlanoSaude() + 
								"\n" +  "Idade : " + umPaciente.getIdade() );
		return stringGigante;
	}
	
	public String buscarConsultaMed(Medico umMed, String umaData, String umHorario) {
		ConsultaMedica umaConsulta = umMed.buscarConsulta(umaData, umHorario);
		for (Paciente umPaciente : listaPaciente) {
			if (umPaciente.buscarConsulta(umaData, umHorario).equalization(umaConsulta)==true){
				return umPaciente.getNome();
			}
		}
		return null;
	}
	
	public String buscarConsultaPac(Paciente umPac, String umaData, String umHorario) {
		ConsultaMedica umaConsulta = umPac.buscarConsulta(umaData, umHorario);
		for (Medico umMed : listaMedico) {
			if (umMed.buscarConsulta(umaData, umHorario).equalization(umaConsulta)==true){
				return umMed.getNome();
			}
		}
		return null;
	}

}

	