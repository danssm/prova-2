package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JEditorPane;

import Controllers.Controller;
import Model.ConsultaMedica;
import Model.Medico;
import Model.Paciente;

public class InterfaceCad extends JFrame {

	private JPanel contentPane;
	private JTextField textNomePac;
	private JTextField textCpfPac;
	private JTextField textRgPac;
	private JTextField textSintoma;
	private JTextField textPlano;
	private JTextField textIdade;
	private JLabel lblNomePac;
	private JLabel lblCpfPac;
	private JLabel lblRgPac;
	private JLabel lblSintoma;
	private JLabel lblPlano;
	private JLabel lblIdade;
	private JButton botaoCadPaciente;
	private JLabel labelSemImportancia;
	private JTextField textNomeMed;
	private JTextField textCpfMed;
	private JTextField textRgMed;
	private JTextField textEspecialidade;
	private JTextField textRegistro;
	private JTextField textDiagnostico;
	private JTextField textData;
	private JTextField textHorario;
	private JButton botaoCadConsulta;
	private JButton botaoBuscarMed;
	private JTextField textUsuario;
	private JLabel lblUsuario;
	private JButton btnListarUsurios;
	private JButton botaoRemover;
	private JButton btnListarFicha;
	private JButton botaoBuscarPac;
	
	private int num;
	private Paciente umPaciente;
	private Medico umMedico;
	private ConsultaMedica umaConsulta;
	Controller umControl = new Controller();
	
	private JButton botaoListarConsultas;
	private JButton botaoFicharConsulta;
	private JTextField textBuscarData;
	private JLabel label;
	private JTextField textBuscarHorario;
	private JLabel label_1;
	private JTextField textPacConsulta;
	private JLabel lblNomeDePaciente;
	private JTextField textMedConsulta;
	private JLabel lblNomeDoMdico;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceCad frame = new InterfaceCad();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfaceCad() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 565);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(238, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 16));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JLabel lblNewLabel = new JLabel("Cadastro de Paciente");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 17));
		lblNewLabel.setBounds(6, 22, 212, 32);
		contentPane.add(lblNewLabel);
		
		textNomePac = new JTextField();
		textNomePac.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textNomePac.setBounds(98, 79, 108, 28);
		contentPane.add(textNomePac);
		textNomePac.setColumns(10);
		
		textCpfPac = new JTextField();
		textCpfPac.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textCpfPac.setColumns(10);
		textCpfPac.setBounds(98, 118, 108, 28);
		contentPane.add(textCpfPac);
		
		textRgPac = new JTextField();
		textRgPac.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textRgPac.setColumns(10);
		textRgPac.setBounds(98, 157, 108, 28);
		contentPane.add(textRgPac);
		
		textSintoma = new JTextField();
		textSintoma.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textSintoma.setColumns(10);
		textSintoma.setBounds(98, 196, 108, 28);
		contentPane.add(textSintoma);
		
		textPlano = new JTextField();
		textPlano.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textPlano.setColumns(10);
		textPlano.setBounds(98, 235, 108, 28);
		contentPane.add(textPlano);
		
		textIdade = new JTextField();
		textIdade.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textIdade.setColumns(10);
		textIdade.setBounds(98, 274, 108, 28);
		contentPane.add(textIdade);
		
		lblNomePac = new JLabel("Nome :");
		lblNomePac.setBounds(35, 84, 61, 16);
		contentPane.add(lblNomePac);
		
		lblCpfPac = new JLabel("CPF :");
		lblCpfPac.setBounds(35, 123, 61, 16);
		contentPane.add(lblCpfPac);
		
		lblRgPac = new JLabel("RG :");
		lblRgPac.setBounds(35, 163, 61, 16);
		contentPane.add(lblRgPac);
		
		lblSintoma = new JLabel("Sintoma :");
		lblSintoma.setBounds(35, 202, 61, 16);
		contentPane.add(lblSintoma);
		
		lblPlano = new JLabel("Plano de Saude :");
		lblPlano.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		lblPlano.setBounds(6, 241, 90, 16);
		contentPane.add(lblPlano);
		
		lblIdade = new JLabel("Idade :");
		lblIdade.setBounds(35, 280, 61, 16);
		contentPane.add(lblIdade);
		
		
		
		labelSemImportancia = new JLabel("Cadastro de Médico");
		labelSemImportancia.setHorizontalAlignment(SwingConstants.CENTER);
		labelSemImportancia.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 17));
		labelSemImportancia.setBounds(235, 22, 212, 32);
		contentPane.add(labelSemImportancia);
		
		textNomeMed = new JTextField();
		textNomeMed.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textNomeMed.setColumns(10);
		textNomeMed.setBounds(327, 79, 108, 28);
		contentPane.add(textNomeMed);
		
		textCpfMed = new JTextField();
		textCpfMed.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textCpfMed.setColumns(10);
		textCpfMed.setBounds(327, 118, 108, 28);
		contentPane.add(textCpfMed);
		
		textRgMed = new JTextField();
		textRgMed.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textRgMed.setColumns(10);
		textRgMed.setBounds(327, 157, 108, 28);
		contentPane.add(textRgMed);
		
		textEspecialidade = new JTextField();
		textEspecialidade.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textEspecialidade.setColumns(10);
		textEspecialidade.setBounds(327, 196, 108, 28);
		contentPane.add(textEspecialidade);
		
		textRegistro = new JTextField();
		textRegistro.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textRegistro.setColumns(10);
		textRegistro.setBounds(327, 235, 108, 28);
		contentPane.add(textRegistro);
		
		final JLabel lblNomeMed = new JLabel("Nome :");
		lblNomeMed.setBounds(264, 84, 61, 16);
		contentPane.add(lblNomeMed);
		
		final JLabel lblCpfMed = new JLabel("CPF :");
		lblCpfMed.setBounds(264, 123, 61, 16);
		contentPane.add(lblCpfMed);
		
		final JLabel lblRgMed = new JLabel("RG :");
		lblRgMed.setBounds(264, 163, 61, 16);
		contentPane.add(lblRgMed);
		
		final JLabel lblEspecialidade = new JLabel("Especialidade :");
		lblEspecialidade.setBounds(226, 202, 99, 16);
		contentPane.add(lblEspecialidade);
		
		final JLabel lblRegistro = new JLabel("Registro Médico :");
		lblRegistro.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		lblRegistro.setBounds(226, 241, 99, 16);
		contentPane.add(lblRegistro);
		
		JLabel lblConsulta = new JLabel("Cadastro de Consulta");
		lblConsulta.setHorizontalAlignment(SwingConstants.CENTER);
		lblConsulta.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 17));
		lblConsulta.setBounds(143, 358, 212, 32);
		contentPane.add(lblConsulta);
		
		textDiagnostico = new JTextField();
		textDiagnostico.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textDiagnostico.setColumns(10);
		textDiagnostico.setBounds(227, 412, 212, 28);
		contentPane.add(textDiagnostico);
		
		textData = new JTextField();
		textData.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textData.setColumns(10);
		textData.setBounds(227, 451, 90, 28);
		contentPane.add(textData);
		
		textHorario = new JTextField();
		textHorario.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textHorario.setColumns(10);
		textHorario.setBounds(227, 490, 69, 28);
		contentPane.add(textHorario);
		
		JLabel lblDiagnostico = new JLabel("Diagnótico :");
		lblDiagnostico.setBounds(147, 417, 78, 16);
		contentPane.add(lblDiagnostico);
		
		JLabel lblData = new JLabel("Data :");
		lblData.setBounds(183, 456, 42, 16);
		contentPane.add(lblData);
		
		JLabel lblHorario = new JLabel("Horário :");
		lblHorario.setBounds(164, 496, 61, 16);
		contentPane.add(lblHorario);
		
		
		botaoBuscarMed = new JButton("Pesquisar Médicos");
		botaoBuscarMed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num = 1;
				
				botaoBuscarPac.setEnabled(false);
				
			}
		});
		botaoBuscarMed.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		botaoBuscarMed.setBounds(489, 66, 129, 29);
		contentPane.add(botaoBuscarMed);
		
		final JEditorPane dtrpnTutorialDeUso = new JEditorPane();
		dtrpnTutorialDeUso.setText("Tutorial de Uso:\n1) Cadastra os Pacientes e os Médicos\n2) Cadastre as consultas entre eles\n3) Remova os usuarios, confira a ficha de cada \num, ou veja as consultas que foram feitas");
		dtrpnTutorialDeUso.setFont(new Font("Lucida Grande", Font.PLAIN, 12));
		dtrpnTutorialDeUso.setBounds(489, 273, 282, 257);
		contentPane.add(dtrpnTutorialDeUso);
		
		textUsuario = new JTextField();
		textUsuario.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textUsuario.setColumns(10);
		textUsuario.setBounds(603, 107, 122, 28);
		contentPane.add(textUsuario);
		
		lblUsuario = new JLabel("Nome :");
		lblUsuario.setBounds(540, 112, 185, 16);
		contentPane.add(lblUsuario);
		
		botaoCadPaciente = new JButton("Cadastrar Paciente");
		botaoCadPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textNomePac.getText()==null || textSintoma.getText()==null) {
					dtrpnTutorialDeUso.setText("  Por favor preencha o Nome e o Sintoma \n do Paciente.");
				} else {
					umPaciente = new Paciente(textNomePac.getText(), textSintoma.getText());
					umPaciente.setCpf(textCpfPac.getText());
					umPaciente.setIdade(textIdade.getText());
					umPaciente.setPlanoSaude(textPlano.getText());
					umPaciente.setRg(textRgPac.getText());
					umPaciente.adicionar(null);
					
					umControl.adicionar(umPaciente);
					
					dtrpnTutorialDeUso.setText("Paciente cadastrado no hospital \n com sucesso!");
					
					textNomePac.setText("");
					textSintoma.setText("");
					textCpfPac.setText("");
					textRgPac.setText("");
					textPlano.setText("");
					textIdade.setText("");
				}
				
				
			}
		});
		botaoCadPaciente.setBounds(35, 314, 151, 32);
		contentPane.add(botaoCadPaciente);
		
		btnListarUsurios = new JButton("Listar Usuários");
		btnListarUsurios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dtrpnTutorialDeUso.setText(umControl.listarUsuarios());
			}
		});
		btnListarUsurios.setBounds(562, 13, 138, 41);
		contentPane.add(btnListarUsurios);
		
		botaoRemover = new JButton("Remover Usuários");
		botaoRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(num==1){
					Medico medico = umControl.buscarMedico(textUsuario.getText());
					dtrpnTutorialDeUso.setText("Médico " + medico.getNome() + " saiu do \n hospital e foi retirado do sistema.");
					umControl.remover(medico);
					textUsuario.setText("");
					botaoBuscarPac.setEnabled(true);
				}if(num==2){
					Paciente paciente = umControl.buscarPaciente(textUsuario.getText());
					dtrpnTutorialDeUso.setText("Paciente " + paciente.getNome() + " não é mais \n nosso cliente e foi retirado \n do sistema.");
					umControl.remover(paciente);
					textUsuario.setText("");
					botaoBuscarMed.setEnabled(true);
				}
			}
		});
		botaoRemover.setBounds(500, 140, 145, 29);
		contentPane.add(botaoRemover);
		
		btnListarFicha = new JButton("Ficha de Usuário");
		btnListarFicha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(num==1){
					Medico medico = umControl.buscarMedico(textUsuario.getText());
					dtrpnTutorialDeUso.setText(umControl.fichaUsuario(medico));
					botaoBuscarPac.setEnabled(true);
				} if(num==2) {
					Paciente paciente = umControl.buscarPaciente(textUsuario.getText());
					dtrpnTutorialDeUso.setText(umControl.fichaUsuario(paciente));
					botaoBuscarMed.setEnabled(true);
				}
			}
		});
		btnListarFicha.setBounds(642, 140, 129, 29);
		contentPane.add(btnListarFicha);
		
		botaoBuscarPac = new JButton("Pesquisar Paciente");
		botaoBuscarPac.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num = 2;
				botaoBuscarMed.setEnabled(false);
			}
		});
		botaoBuscarPac.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		botaoBuscarPac.setBounds(630, 66, 129, 29);
		contentPane.add(botaoBuscarPac);
		
		JButton botaoCadMedico = new JButton("Cadastrar Médico");
		botaoCadMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				umMedico = new Medico(textNomeMed.getText(), textEspecialidade.getText());
				umMedico.setCpf(textCpfMed.getText());
				umMedico.setRegistroMedico(textRegistro.getText());
				umMedico.setRg(textRgMed.getText());
				umMedico.adicionar(null);
				
				umControl.adicionar(umMedico);
				
				dtrpnTutorialDeUso.setText("Novo médico cadastrado no \n hospital!");
				
				textNomeMed.setText("");
				textEspecialidade.setText("");
				textCpfMed.setText("");
				textRgMed.setText("");
				textRegistro.setText("");
				
			}
		});
		botaoCadMedico.setBounds(264, 314, 151, 32);
		contentPane.add(botaoCadMedico);
		
		botaoCadConsulta = new JButton("Cadastrar Consulta");
		botaoCadConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Medico umMed = umControl.buscarMedico(textMedConsulta.getText());
				Paciente umPac = umControl.buscarPaciente(textPacConsulta.getText());
				umaConsulta = new ConsultaMedica(textDiagnostico.getText());
				umaConsulta.setData(textData.getText());
				umaConsulta.setHorario(textHorario.getText());
				umMed.adicionar(umaConsulta);
				umPac.adicionar(umaConsulta);
				int nMed = umControl.qualPosicaoMed(textMedConsulta.getText());
				int nPac = umControl.qualPosicaoPac(textPacConsulta.getText());
				umControl.reAdicionar(nMed, umMed);
				umControl.reAdicionar(nPac, umPac);
				dtrpnTutorialDeUso.setText("Consulta realizada entre o doutor " + textMedConsulta.getText() + "\n e paciente " + textPacConsulta.getText());
			}
		});
		botaoCadConsulta.setBounds(326, 488, 151, 32);
		contentPane.add(botaoCadConsulta);
		
		botaoListarConsultas = new JButton("Listar Consultas");
		botaoListarConsultas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(num==1) {
					Medico medico = umControl.buscarMedico(textUsuario.getText());
					dtrpnTutorialDeUso.setText(medico.listarConsultas());
				}if(num==2){
					Paciente paciente = umControl.buscarPaciente(textUsuario.getText());
					dtrpnTutorialDeUso.setText(paciente.listarConsultas());
				}
			}
		});
		botaoListarConsultas.setBounds(574, 171, 145, 29);
		contentPane.add(botaoListarConsultas);
		
		botaoFicharConsulta = new JButton("Ficha da Consulta");
		botaoFicharConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(num==1) {
					Medico medico = umControl.buscarMedico(textUsuario.getText());
					ConsultaMedica Consulta = medico.buscarConsulta(textBuscarData.getText(), textBuscarHorario.getText());
					String umNome = umControl.buscarConsultaMed(medico, textBuscarData.getText(), textBuscarHorario.getText());
					String grande = medico.ficharConsulta(Consulta);
					grande = ("Consulta do doutor " + medico.getNome() + "com o paciente " + umNome + "\n" + grande );
					dtrpnTutorialDeUso.setText(grande);
				}if(num==2){
					Paciente paciente = umControl.buscarPaciente(textUsuario.getText());
					ConsultaMedica consulta = paciente.buscarConsulta(textBuscarData.getText(), textBuscarHorario.getText());
					String umNome = umControl.buscarConsultaPac(paciente, textBuscarData.getText(), textBuscarHorario.getText());
					String grande = paciente.ficharConsulta(consulta);
					grande = ("Consulta do doutor " + paciente.getNome() + "com o paciente " + umNome + "\n" + grande );
					dtrpnTutorialDeUso.setText(grande);
				}
				textUsuario.setText("");
				textBuscarData.setText("");
				textBuscarHorario.setText("");
				num=0;
			}
		});
		botaoFicharConsulta.setBounds(553, 241, 147, 29);
		contentPane.add(botaoFicharConsulta);
		
		textBuscarData = new JTextField();
		textBuscarData.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textBuscarData.setColumns(10);
		textBuscarData.setBounds(540, 206, 90, 28);
		contentPane.add(textBuscarData);
		
		label = new JLabel("Data :");
		label.setBounds(496, 211, 42, 16);
		contentPane.add(label);
		
		textBuscarHorario = new JTextField();
		textBuscarHorario.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textBuscarHorario.setColumns(10);
		textBuscarHorario.setBounds(702, 206, 69, 28);
		contentPane.add(textBuscarHorario);
		
		label_1 = new JLabel("Horário :");
		label_1.setBounds(639, 212, 61, 16);
		contentPane.add(label_1);
		
		textPacConsulta = new JTextField();
		textPacConsulta.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textPacConsulta.setColumns(10);
		textPacConsulta.setBounds(27, 417, 108, 28);
		contentPane.add(textPacConsulta);
		
		lblNomeDePaciente = new JLabel("Nome do Paciente:");
		lblNomeDePaciente.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		lblNomeDePaciente.setBounds(35, 398, 100, 16);
		contentPane.add(lblNomeDePaciente);
		
		textMedConsulta = new JTextField();
		textMedConsulta.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		textMedConsulta.setColumns(10);
		textMedConsulta.setBounds(31, 488, 108, 28);
		contentPane.add(textMedConsulta);
		
		lblNomeDoMdico = new JLabel("Nome do Médico:");
		lblNomeDoMdico.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		lblNomeDoMdico.setBounds(35, 473, 100, 16);
		contentPane.add(lblNomeDoMdico);
	}
}
